[![pipeline status](https://gitlab.com/jone.velickaite/jone-velickaite-task/badges/main/pipeline.svg)](https://gitlab.com/jone.velickaite/jone-velickaite-task/-/commits/main)

## Jone Velickaite Practical Skill Check

### To run locally

1. Build the image:

```
docker build -t <image_name> .
```

2. Run the container:

```
docker run -d -p 8080:80 <image_name>
```